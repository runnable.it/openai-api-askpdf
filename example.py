"""
Set environment variable OPENAI_API_KEY to your OpenAI API key.
Set environment variable OPENAI_PROXY to your OpenAI proxy setting, 
such as "http://<proxy_server>:<proxy_port>" or "socks5://<proxy_server>:<proxy_port>" .
"""
from openai_api_askpdf import PaperReader

def main():
    # OPENAI_KEY = os.environ.get("OPENAI_API_KEY")
    # OPENAI_PROXY = os.environ.get("OPENAI_PROXY")
    OPENAI_KEY = None
    OPENAI_PROXY = None
    
    # Creating a PaperReader object
    paper_reader = PaperReader(openai_key=OPENAI_KEY, openai_proxy=OPENAI_PROXY, verbose=True)
    # Reading a research paper from a PDF file and summarizing it
    # Only summary the first page
    selected_pages = [0]
    summary = paper_reader.read_pdf_and_summarize('./alexnet.pdf', selected_pages=selected_pages)
    # Printing the summary of the whole paper
    print(summary)
    # Asking a question after summarizing the paper
    question = 'What is the main contribution of this paper?'
    response = paper_reader.question(question)
    # Printing the system response to the question
    print(response)

if __name__ == "__main__":
    main()

