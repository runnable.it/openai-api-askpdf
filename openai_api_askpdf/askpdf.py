import os
from PyPDF2 import PdfReader
from typing import List
from tqdm import tqdm
# import openai
from openai_api_wrapper import ChatBot

BASE_POINTS = """
1. Who are the authors?
2. What is the process of the proposed method?
3. What is the performance of the proposed method? Please note down its performance metrics.
4. What are the baseline models and their performances? Please note down these baseline methods.
5. What dataset did this paper use?
"""

READING_PROMT_V2 = """
You are a researcher helper bot. You can help the user with research paper reading and summarizing. \n
Now I am going to send you a paper. You need to read it and summarize it for me part by part. \n
You should output both English and Chinese versions at the same time.\n
When you are reading, You need to focus on these key points:{},

And You need to generate a brief but informative title for this part.
Your return format:
- title: '...'
- summary: '...'
"""

class OpenAIModel(object):

    def __init__(self, api_key, openai_proxy=None, model='gpt-3.5-turbo', temperature=0.2) -> None:
        openai.api_key = api_key or os.environ.get("OPENAI_API_KEY")
        openai.proxy = openai_proxy or os.environ.get("OPENAI_PROXY")
        self.model = model
        self.temperature = temperature

    def send_msg(self, msg: List[dict], return_raw_text=True):
        
        response = openai.ChatCompletion.create(
            model=self.model,
            messages=msg,
            temperature=self.temperature
        )

        if return_raw_text:
            return response["choices"][0]["message"]["content"]
        else:
            return response
            
class Paper(object):

    def __init__(self, pdf_obj: PdfReader) -> None:
        self._pdf_obj = pdf_obj
        self._paper_meta = self._pdf_obj.metadata

    def iter_pages(self, iter_text_len: int = 3000):
        page_idx = 0
        for page in self._pdf_obj.pages:
            txt = page.extract_text()
            for i in range((len(txt) // iter_text_len) + 1):
                yield page_idx, i, txt[i * iter_text_len:(i + 1) * iter_text_len]
            page_idx += 1

class PaperReader:

    """
    A class for summarizing research papers using the OpenAI API.

    Attributes:
        openai_key (str): The API key to use the OpenAI API.
        token_length (int): The length of text to send to the API at a time.
        model (str): The GPT model to use for summarization.
        points_to_focus (str): The key points to focus on while summarizing.
        verbose (bool): A flag to enable/disable verbose logging.

    """

    def __init__(self, openai_key, openai_proxy=None, token_length=4000, model="gpt-3.5-turbo",
                 points_to_focus=BASE_POINTS, verbose=False):

        # Initializing prompts for the conversation
        self.init_prompt = READING_PROMT_V2.format(points_to_focus)

        self.summary_prompt = 'You are a researcher helper bot. Now you need to read the summaries of a research paper.'
        # self.messages = []  # Initializing the conversation messages
        self.summary_msg = []  # Initializing the summary messages
        self.token_len = token_length  # Setting the token length to use
        self.keep_round = 2  # Rounds of previous dialogues to keep in conversation
        # self.model = model  # Setting the GPT model to use
        self.verbose = verbose  # Flag to enable/disable verbose logging
        # self.model = OpenAIModel(api_key=openai_key, openai_proxy=openai_proxy, model=model)
        self.model = ChatBot(api_key=openai_key, proxy=openai_proxy, engine=model, system_prompt=self.init_prompt)

    # def drop_conversation(self, msg):
    #     # This method is used to drop previous messages from the conversation and keep only recent ones
    #     self.model.remove_conversation()
    #     if len(msg) >= (self.keep_round + 1) * 2 + 1:
    #         new_msg = [msg[0]]
    #         for i in range(3, len(msg)):
    #             new_msg.append(msg[i])
    #         return new_msg
    #     else:
    #         return msg

    def send_msg(self, msg):
        # return self.model.send_msg(msg)
        return self.model.send_msg(msg)

    # def _chat(self, message):
    #     # This method is used to send a message and get a response from the OpenAI API

    #     # Adding the user message to the conversation messages
    #     self.messages.append({"role": "user", "content": message})
    #     # Sending the messages to the API and getting the response
    #     response = self.send_msg(self.messages)
    #     # Adding the system response to the conversation messages
    #     self.messages.append({"role": "system", "content": response})
    #     # Dropping previous conversation messages to keep the conversation history short
    #     self.messages = self.drop_conversation(self.messages)
    #     # Returning the system response
    #     return response

    def summarize(self, paper: Paper, selected_pages: List[int] = None):
        # This method is used to summarize a given research paper

        # Adding the initial prompt to the conversation messages
        # self.messages = [
        #     {"role": "system", "content": self.init_prompt},
        # ]
        # Adding the summary prompt to the summary messages
        self.summary_msg = [{"role": "system", "content": self.init_prompt}]

         # Reading and summarizing each part of the research paper
        for (page_idx, part_idx, text) in tqdm(paper.iter_pages(), ncols=100, desc="PDF Pages"):
            # FIXME
            if selected_pages and page_idx not in selected_pages:
                continue

            print('page: {}, part: {}'.format(page_idx, part_idx))
            # Sending the text to the API and getting the response
            # summary = self._chat('now I send you page {}, part {}：{}'.format(page_idx, part_idx, text))
            summary = self.model.chat('now I send you page {}, part {}：{}'.format(page_idx, part_idx, text))
            # Logging the summary if verbose logging is enabled
            if self.verbose:
                print(summary)
            # Adding the summary of the part to the summary messages
            self.summary_msg.append({"role": "user", "content": '{}'.format(summary)})

        # Adding a prompt for the user to summarize the whole paper to the summary messages
        self.summary_msg.append({"role": "user", "content": 'Now please make a summary of the whole paper'})
        # Sending the summary messages to the API and getting the response
        result = self.send_msg(self.summary_msg)
        # Returning the summary of the whole paper
        return result

    def read_pdf_and_summarize(self, pdf_path, selected_pages: List[int] = None):
        # This method is used to read a research paper from a PDF file and summarize it
        
        # Creating a PdfReader object to read the PDF file
        pdf_reader = PdfReader(pdf_path)
        paper = Paper(pdf_reader)
        # Summarizing the full text of the research paper and returning the summary
        print('reading pdf finished')
        summary = self.summarize(paper, selected_pages=selected_pages)
        return summary

    def get_summary_of_each_part(self):
        # This method is used to get the summary of each part of the research paper
        return self.summary_msg

    def question(self, question):
        # This method is used to ask a question after summarizing a paper

        # Adding the question to the summary messages
        self.summary_msg.append({"role": "user", "content": question})
        # Sending the summary messages to the API and getting the response
        response = self.send_msg(self.summary_msg)
        # Adding the system response to the summary messages
        self.summary_msg.append({"role": "system", "content": response})
        # Returning the system response
        return response

if __name__ == '__main__':
    # OPENAI_KEY = os.environ.get("OPENAI_API_KEY")
    # OPENAI_PROXY = os.environ.get("OPENAI_PROXY")
    
    # Creating a PaperReader object
    paper_reader = PaperReader(openai_key=OPENAI_KEY, openai_proxy=OPENAI_PROXY, verbose=True)
    # Reading a research paper from a PDF file and summarizing it
    summary = paper_reader.read_pdf_and_summarize('../alexnet.pdf')
    # Printing the summary of the whole paper
    print(summary)
    # Asking a question after summarizing the paper
    question = 'What is the main contribution of this paper?'
    response = paper_reader.question(question)
    # Printing the system response to the question
    print(response)
    