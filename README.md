
OpenAI API AskPDF
=================

OpenAI API AskPDF is an open source project that allows users to extract abstracts from English papers and generate answers to questions based on the content of the paper. The project utilizes the GPT-3.5 architecture, which is a state-of-the-art language model, to generate text. With the OpenAI API, users can access the power of GPT-3.5 and leverage its capabilities to extract insights from scientific papers.

Scientific papers are an essential source of knowledge in various fields, from biology to physics and computer science. However, reading and understanding these papers can be time-consuming and challenging, especially for non-experts. OpenAI API AskPDF provides a solution to this problem by enabling users to extract abstracts from papers and generate answers to questions based on the content of the paper. With this project, users can quickly gain insights into a paper's key findings and concepts without having to read the entire paper.

Requirements
------------

To use OpenAI API AskPDF, you will need:

*  Python 3.6 or higher
*  OpenAI API key
*  `openai` Python package

Installation
------------

1. Clone this repository to your local machine.
2. Install the `openai` package by running `pip install openai` in your terminal.
3. Set the `OPENAI_API_KEY` environment variable to your OpenAI API key.

Usage
-----

OpenAI API AskPDF offers two main functions: extracting abstracts from a paper and generating answers to questions based on the content of the paper.

### Extracting Abstracts

To extract the abstracts from a paper, follow these steps:

1.  Download the PDF file of the paper you want to extract abstracts from.
2.  In your terminal, navigate to the directory where you cloned this repository.
3.  Run the `extract_abstracts.py` script with the following command: `python extract_abstracts.py <path-to-pdf>`

The script will extract the abstracts and save them as separate text files in the `abstracts` folder.

### Generating Answers to Questions

To generate answers to questions based on the content of a paper, follow these steps:

1. Download the PDF file of the paper you want to generate answers from.
2. In your terminal, navigate to the directory where you cloned this repository.
3. Run the `ask_questions.py` script with the following command: `python ask_questions.py <path-to-paper>`.
4. The script will prompt you to enter a question.
5. Enter your question and press enter.
6. The script will generate an answer based on the content of the paper.

Limitations
-----------

The project currently only works with English papers, and may not be accurate for papers with complex technical language or terminology. Additionally, while the OpenAI API is free to use, it does require an API key, which may have usage limits depending on your plan.

Contributing
------------

Contributions to this project are welcome. If you find any bugs or have suggestions for improvements, please open an issue or submit a pull request.

License
-------

This project is licensed under the MIT License. See the LICENSE file for details.
