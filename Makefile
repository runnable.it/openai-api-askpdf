PACKAGENAME=`grep -e '^NAME = ' setup.py | cut -d ' ' -f 3 | sed -e "s/'//g"`

all: clean buildall

buildall: clean
	python setup.py sdist bdist_wheel

upload-test: buildall
	twine upload --repository-url https://test.pypi.org/legacy/ dist/*

upload: buildall
	twine upload --verbose dist/*

install-test:
	pip install --index-url https://test.pypi.org/simple/ ${PACKAGENAME}

install:
	pip install --index-url https://pypi.org/simple/ ${PACKAGENAME}

clean:
	rm -fr `find . -name "__pycache__"` `find . -name "*.pyc"` setup.pye setup.pyn build dist *.egg-info

example:
	python example.py
